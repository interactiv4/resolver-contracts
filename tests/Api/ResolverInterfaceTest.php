<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\Api;

use Interactiv4\Contracts\Resolver\Api\ResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;
use PHPUnit\Framework\TestCase;

/**
 * Class ResolverInterfaceTest.
 *
 * @internal
 */
class ResolverInterfaceTest extends TestCase
{
    use ResolverInterfaceTestTrait;

    /**
     * @return array
     */
    public function resolveDataProvider(): array
    {
        $array = [1, '2', 3.3, false];
        $iterator = new \ArrayIterator($array);
        $bool = true;
        $callableValid = [$this, 'resolveDataProvider'];
        $callableInvalid = [$this, 'nonExistingMethod'];
        $closureValid = \Closure::fromCallable($callableValid);
        $float = 999.99;
        $int = 123;
        $object = new \stdClass();
        $string = '12345abcde';

        return [
            'null' => [null, null, false],
            'array' => [$array, $array, false],
            'array - iterable' => [$iterator, $iterator, false],
            'bool' => [$bool, $bool, false],
            'callable - valid' => [$callableValid, $callableValid, false],
            'callable - invalid' => [$callableInvalid, $callableInvalid, false],
            'callable - closure' => [$closureValid, $closureValid, false],
            'float' => [$float, $float, false],
            'int' => [$int, $int, false],
            'iterable - iterable' => [$iterator, $iterator, false],
            'iterable - array' => [$array, $array, false],
            'object' => [$object, $object, false],
            'string' => [$string, $string, false],
        ];
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return string
     */
    private function getResolverClass(): string
    {
        return ProvidedValueResolver::class;
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return array
     */
    private function getResolverInterfaces(): array
    {
        return [
            ResolverInterface::class,
        ];
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return mixed|null
     */
    private function getDefaultValue()
    {
        return null;
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return mixed|null
     */
    private function getExpectedDefaultValue()
    {
        return $this->getDefaultValue();
    }
}
