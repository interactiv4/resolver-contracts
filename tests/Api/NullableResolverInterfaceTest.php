<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\Api;

use Interactiv4\Contracts\Resolver\Api\NullableResolverInterface;
use Interactiv4\Contracts\Resolver\Api\ResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\NullableProvidedValueResolver;

/**
 * Class NullableResolverInterfaceTest.
 *
 * @internal
 */
class NullableResolverInterfaceTest extends ResolverInterfaceTest
{
    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return string
     */
    private function getResolverClass(): string
    {
        return NullableProvidedValueResolver::class;
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return array
     */
    private function getResolverInterfaces(): array
    {
        return [
            NullableResolverInterface::class,
            ResolverInterface::class,
        ];
    }
}
