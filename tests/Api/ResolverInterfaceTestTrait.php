<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\Api;

use Interactiv4\Contracts\Resolver\Api\ResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;
use PHPUnit\Framework\MockObject\MockObject;
use TypeError;

/**
 * Trait ResolverInterfaceTestTrait.
 */
trait ResolverInterfaceTestTrait
{
    /**
     * @var ResolverInterface|MockObject
     */
    private $resolver;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->resolver = $this->getMockBuilder($this->getResolverClass())
            ->enableOriginalConstructor()
            ->setConstructorArgs([$this->getDefaultValue()])
            ->setMethods()
            ->getMock()
        ;
    }

    /**
     * Test resolver is an instance of resolver interface.
     */
    public function testInstanceOf(): void
    {
        foreach ($this->getResolverInterfaces() as $resolverInterface) {
            $this->assertInstanceOf(
                $resolverInterface,
                $this->resolver
            );
        }
    }

    /**
     * Test resolve method.
     * Due to some automatic casting, provided value sometimes get casted to returned type
     * Provide also expected value.
     *
     * @param $providedValue
     * @param $expectedValue
     * @param bool $expectTypeError
     *
     * @dataProvider resolveDataProvider
     */
    public function testResolve(
        $providedValue,
        $expectedValue,
        bool $expectTypeError
    ): void {
        // Test callable without args, it returns default value without type error
        $this->assertSame(
            $this->getExpectedDefaultValue(),
            $this->resolver->resolve()
        );

        // Test callable with args, it returns provided value with expected type error
        if ($expectTypeError) {
            $this->expectException(TypeError::class);
        }

        $this->assertSame(
            $expectedValue,
            $this->resolver->resolve([ProvidedValueResolver::PROVIDED_VALUE_KEY => $providedValue])
        );
    }

    /**
     * @throws \Exception
     *
     * @return string
     */
    private function getResolverClass(): string
    {
        throw new \Exception(__METHOD__ . ' must be overriden in test file');
    }

    /**
     * @throws \Exception
     *
     * @return array
     */
    private function getResolverInterfaces(): array
    {
        throw new \Exception(__METHOD__ . ' must be overriden in test file');
    }

    /**
     * @throws \Exception
     *
     * @return mixed|null
     */
    private function getDefaultValue()
    {
        throw new \Exception(__METHOD__ . ' must be overriden in test file');
    }

    /**
     * @throws \Exception
     *
     * @return mixed|null
     */
    private function getExpectedDefaultValue()
    {
        throw new \Exception(__METHOD__ . ' must be overriden in test file');
    }
}
