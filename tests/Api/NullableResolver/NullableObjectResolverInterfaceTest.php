<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\Api\NullableResolver;

use Interactiv4\Contracts\Resolver\Api\NullableResolver\NullableObjectResolverInterface;
use Interactiv4\Contracts\Resolver\Api\NullableResolverInterface;
use Interactiv4\Contracts\Resolver\Api\ResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\NullableResolver\NullableObjectProvidedValueResolver;
use Interactiv4\Contracts\Resolver\Test\Api\ResolverInterfaceTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class NullableObjectResolverInterfaceTest.
 *
 * @internal
 */
class NullableObjectResolverInterfaceTest extends TestCase
{
    use ResolverInterfaceTestTrait;

    /**
     * @var object
     */
    private $defaultValue;

    /**
     * @return array
     */
    public function resolveDataProvider(): array
    {
        $array = [1, '2', 3.3, true];
        $iterator = new \ArrayIterator($array);
        $bool = true;
        $callableValid = [$this, 'resolveDataProvider'];
        $callableInvalid = [$this, 'nonExistingMethod'];
        $closureValid = \Closure::fromCallable($callableValid);
        $float = 999.99;
        $int = 123;
        $object = new \stdClass();
        $string = '12345abcde';

        return [
            'null' => [null, null, false],
            'array' => [$array, $array, true],
            'bool' => [$bool, $bool, true],
            'callable - valid' => [$callableValid, $callableValid, true],
            'callable - invalid' => [$callableInvalid, $callableInvalid, true],
            'callable - closure' => [$closureValid, $closureValid, false],
            'float' => [$float, $float, true],
            'int' => [$int, $int, true],
            'iterable' => [$iterator, $iterator, false],
            'object' => [$object, $object, false],
            'string' => [$string, $string, true],
        ];
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return string
     */
    private function getResolverClass(): string
    {
        return NullableObjectProvidedValueResolver::class;
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return array
     */
    private function getResolverInterfaces(): array
    {
        return [
            NullableObjectResolverInterface::class,
            NullableResolverInterface::class,
            ResolverInterface::class,
        ];
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return mixed|null
     */
    private function getDefaultValue()
    {
        return $this->defaultValue ?? $this->defaultValue = new \stdClass();
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return mixed|null
     */
    private function getExpectedDefaultValue()
    {
        return $this->getDefaultValue();
    }
}
