<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\Api\NullableResolver;

use Interactiv4\Contracts\Resolver\Api\NullableResolver\NullableBoolResolverInterface;
use Interactiv4\Contracts\Resolver\Api\NullableResolverInterface;
use Interactiv4\Contracts\Resolver\Api\ResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\NullableResolver\NullableBoolProvidedValueResolver;
use Interactiv4\Contracts\Resolver\Test\Api\ResolverInterfaceTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class NullableBoolResolverInterfaceTest.
 *
 * @internal
 */
class NullableBoolResolverInterfaceTest extends TestCase
{
    use ResolverInterfaceTestTrait;

    /**
     * @return array
     */
    public function resolveDataProvider(): array
    {
        $array = [1, '2', 3.3, true];
        $iterator = new \ArrayIterator($array);
        $bool = true;
        $callableValid = [$this, 'resolveDataProvider'];
        $callableInvalid = [$this, 'nonExistingMethod'];
        $closureValid = \Closure::fromCallable($callableValid);
        $float = 999.99;
        $int = 123;
        $object = new \stdClass();
        $string = '12345abcde';

        return [
            'null' => [null, null, false],
            'array' => [$array, $array, true],
            'bool' => [$bool, $bool, false],
            'callable - valid' => [$callableValid, $callableValid, true],
            'callable - invalid' => [$callableInvalid, $callableInvalid, true],
            'callable - closure' => [$closureValid, $closureValid, true],
            'float' => [$float, $float, true],
            'int' => [$int, $int, true],
            'iterable' => [$iterator, $iterator, true],
            'object' => [$object, $object, true],
            'string' => [$string, $string, true],
        ];
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return string
     */
    private function getResolverClass(): string
    {
        return NullableBoolProvidedValueResolver::class;
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return array
     */
    private function getResolverInterfaces(): array
    {
        return [
            NullableBoolResolverInterface::class,
            NullableResolverInterface::class,
            ResolverInterface::class,
        ];
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return mixed|null
     */
    private function getDefaultValue()
    {
        return true;
    }

    /**
     * Used in trait, do not remove.
     *
     * @throws \Exception
     *
     * @return mixed|null
     */
    private function getExpectedDefaultValue()
    {
        return $this->getDefaultValue();
    }
}
