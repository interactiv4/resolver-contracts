<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\Resolver;

use Interactiv4\Contracts\Resolver\Api\Resolver\IterableResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class IterableProvidedValueResolver.
 */
class IterableProvidedValueResolver extends ProvidedValueResolver implements IterableResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): iterable
    {
        /* @var iterable $value */
        return parent::resolve($arguments);
    }
}
