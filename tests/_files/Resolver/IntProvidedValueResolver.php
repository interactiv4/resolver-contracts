<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\Resolver;

use Interactiv4\Contracts\Resolver\Api\Resolver\IntResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class IntProvidedValueResolver.
 */
class IntProvidedValueResolver extends ProvidedValueResolver implements IntResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): int
    {
        /* @var int $value */
        return parent::resolve($arguments);
    }
}
