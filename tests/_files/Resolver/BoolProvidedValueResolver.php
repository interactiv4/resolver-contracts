<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\Resolver;

use Interactiv4\Contracts\Resolver\Api\Resolver\BoolResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class BoolProvidedValueResolver.
 */
class BoolProvidedValueResolver extends ProvidedValueResolver implements BoolResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): bool
    {
        /* @var bool $value */
        return parent::resolve($arguments);
    }
}
