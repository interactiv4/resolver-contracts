<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\Resolver;

use Interactiv4\Contracts\Resolver\Api\Resolver\StringResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class StringProvidedValueResolver.
 */
class StringProvidedValueResolver extends ProvidedValueResolver implements StringResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): string
    {
        /* @var string $value */
        return parent::resolve($arguments);
    }
}
