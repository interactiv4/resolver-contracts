<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\Resolver;

use Interactiv4\Contracts\Resolver\Api\Resolver\CallableResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class CallableProvidedValueResolver.
 */
class CallableProvidedValueResolver extends ProvidedValueResolver implements CallableResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): callable
    {
        /* @var callable $value */
        return parent::resolve($arguments);
    }
}
