<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\Resolver;

use Interactiv4\Contracts\Resolver\Api\Resolver\ArrayResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class ArrayProvidedValueResolver.
 */
class ArrayProvidedValueResolver extends ProvidedValueResolver implements ArrayResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): array
    {
        /* @var array $value */
        return parent::resolve($arguments);
    }
}
