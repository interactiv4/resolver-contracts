<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\Resolver;

use Interactiv4\Contracts\Resolver\Api\Resolver\ObjectResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class ObjectProvidedValueResolver.
 */
class ObjectProvidedValueResolver extends ProvidedValueResolver implements ObjectResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): object
    {
        /* @var object $value */
        return parent::resolve($arguments);
    }
}
