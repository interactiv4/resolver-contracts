<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\Resolver;

use Interactiv4\Contracts\Resolver\Api\Resolver\FloatResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class FloatProvidedValueResolver.
 */
class FloatProvidedValueResolver extends ProvidedValueResolver implements FloatResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): float
    {
        /* @var float $value */
        return parent::resolve($arguments);
    }
}
