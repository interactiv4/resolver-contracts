<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\NullableResolver;

use Interactiv4\Contracts\Resolver\Api\NullableResolver\NullableBoolResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class NullableBoolProvidedValueResolver.
 */
class NullableBoolProvidedValueResolver extends ProvidedValueResolver implements NullableBoolResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): ?bool
    {
        /* @var bool|null $value */
        return parent::resolve($arguments);
    }
}
