<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\NullableResolver;

use Interactiv4\Contracts\Resolver\Api\NullableResolver\NullableCallableResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class NullableCallableProvidedValueResolver.
 */
class NullableCallableProvidedValueResolver extends ProvidedValueResolver implements NullableCallableResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): ?callable
    {
        /* @var callable|null $value */
        return parent::resolve($arguments);
    }
}
