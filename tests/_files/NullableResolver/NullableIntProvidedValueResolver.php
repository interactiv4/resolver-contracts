<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\NullableResolver;

use Interactiv4\Contracts\Resolver\Api\NullableResolver\NullableIntResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class NullableIntProvidedValueResolver.
 */
class NullableIntProvidedValueResolver extends ProvidedValueResolver implements NullableIntResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): ?int
    {
        /* @var int|null $value */
        return parent::resolve($arguments);
    }
}
