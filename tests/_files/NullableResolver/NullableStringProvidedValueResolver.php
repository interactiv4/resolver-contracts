<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\NullableResolver;

use Interactiv4\Contracts\Resolver\Api\NullableResolver\NullableStringResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class NullableStringProvidedValueResolver.
 */
class NullableStringProvidedValueResolver extends ProvidedValueResolver implements NullableStringResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): ?string
    {
        /* @var string|null $value */
        return parent::resolve($arguments);
    }
}
