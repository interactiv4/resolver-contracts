<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files\NullableResolver;

use Interactiv4\Contracts\Resolver\Api\NullableResolver\NullableArrayResolverInterface;
use Interactiv4\Contracts\Resolver\Test\_files\ProvidedValueResolver;

/**
 * Class NullableArrayProvidedValueResolver.
 */
class NullableArrayProvidedValueResolver extends ProvidedValueResolver implements NullableArrayResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): ?array
    {
        /* @var array|null $value */
        return parent::resolve($arguments);
    }
}
