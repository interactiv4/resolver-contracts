<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files;

use Interactiv4\Contracts\Resolver\Api\ResolverInterface;

/**
 * Class ProvidedValueResolver.
 */
class ProvidedValueResolver implements ResolverInterface
{
    const PROVIDED_VALUE_KEY = 'provided_value';

    /**
     * @var mixed
     */
    private $defaultValue;

    /**
     * ProvidedValueResolver constructor.
     *
     * @param mixed $defaultValue
     */
    public function __construct(
        $defaultValue
    ) {
        $this->defaultValue = $defaultValue;
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = [])
    {
        return \array_key_exists(self::PROVIDED_VALUE_KEY, $arguments)
            ? $arguments[self::PROVIDED_VALUE_KEY]
            : $this->defaultValue;
    }
}
