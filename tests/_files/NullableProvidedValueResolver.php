<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Resolver\Test\_files;

use Interactiv4\Contracts\Resolver\Api\NullableResolverInterface;

/**
 * Class NullableProvidedValueResolver.
 */
class NullableProvidedValueResolver extends ProvidedValueResolver implements NullableResolverInterface
{
}
