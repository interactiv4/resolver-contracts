<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Resolver\Api\Resolver;

use Interactiv4\Contracts\Resolver\Api\ResolverInterface;

/**
 * Interface BoolResolverInterface.
 *
 * @api
 */
interface BoolResolverInterface extends ResolverInterface
{
    /**
     * Redefine strict return type.
     *
     * {@inheritdoc}
     *
     * @return bool
     */
    public function resolve(array $arguments = []): bool;
}
