<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Resolver\Api\NullableResolver;

use Interactiv4\Contracts\Resolver\Api\NullableResolverInterface;

/**
 * Interface NullableIterableResolverInterface.
 *
 * @api
 */
interface NullableIterableResolverInterface extends NullableResolverInterface
{
    /**
     * Redefine strict return type.
     *
     * {@inheritdoc}
     *
     * @return iterable|null
     */
    public function resolve(array $arguments = []): ?iterable;
}
