<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Resolver\Api\NullableResolver;

use Interactiv4\Contracts\Resolver\Api\NullableResolverInterface;

/**
 * Interface NullableStringResolverInterface.
 *
 * @api
 */
interface NullableStringResolverInterface extends NullableResolverInterface
{
    /**
     * Redefine strict return type.
     *
     * {@inheritdoc}
     *
     * @return string|null
     */
    public function resolve(array $arguments = []): ?string;
}
