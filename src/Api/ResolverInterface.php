<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Resolver\Api;

/**
 * Interface ResolverInterface.
 *
 * @api
 */
interface ResolverInterface
{
    /**
     * Resolve value from optionally specified parameters.
     *
     * This interfaces aim to unify resolvers signature.
     * Resolvers implementing this interface know to interpret supplied parameters, and resolve a value.
     *
     * Missing strict return type on purpose; if defined, this return type cannot be
     * overridden by implementing classes, even if specified return type is compatible with original one.
     *
     * Allow classes implementing this interface to redefine strict return type value.
     *
     * @param array $arguments
     *
     * @throws \InvalidArgumentException
     *
     * @return mixed|null
     */
    public function resolve(array $arguments = []);
}
