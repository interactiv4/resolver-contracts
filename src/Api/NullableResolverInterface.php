<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Resolver\Api;

/**
 * Interface NullableResolverInterface.
 *
 * Marker class for ResolverInterface instances that allow null as return type
 *
 * @api
 */
interface NullableResolverInterface extends ResolverInterface
{
}
